var shop = [{
              name: 'damage',
              price: 100,
              limit: 10,
              type: 'upgrade'
            },
            {
              name: 'shoots',
              price: 40,
              limit: null,
              type: 'upgrade'
            },
            {
              name: 'speed',
              price: 50,
              limit: 3,
              type: 'upgrade'
            },
            {
              name: 'health',
              price: 40,
              limit: 15,
              type: 'upgrade'
            },
            {
              name: 'tv',
              price: 500,
              type: 'furniture'
            },
            {
              name: 'chair',
              price: 200,
              type: 'furniture'
            },
            {
              name: 'ruby',
              price: 100000,
              type: 'furniture'
            },
            {
              name: 'couch',
              price: 500,
              type: 'furniture'
            },
            {
              name: 'vase',
              price: 100,
              type: 'furniture'
            }];
say('Hello player.');
say('Welcome to our shop!');
var item = shop[this.properties.item];
switch (ask('Did you want to buy a ' + item.name + '-' + item.type + '? The ' + item.type + ' cost ' + item.price + ' coins.')) {
  case 'yes':
    if (item.price <= gameStorage.coins &&
        (item.price * gameStorage.upgrades[this.properties.item] <= gameStorage.coins ||
         item.type !== 'upgrade')) {
      switch (item.type) {
        case 'upgrade':
          if (!item.limit || gameStorage.upgrades[this.properties.item] < item.limit) {
            gameStorage.coins -= item.price * gameStorage.upgrades[this.properties.item];
            gameStorage.upgrades[this.properties.item]++;
            say('The upgrade is now bought.');
          } else {
            say('You cannot upgrade the upgrade, because the upgrade is limited.');
          }
          break;
        case 'furniture':
          if (gameStorage.furnitures.indexOf(item.name) === -1) {
            gameStorage.coins -= item.price;
            gameStorage.furnitures.push(item.name);
            say('The furniture is now bought.');
          } else {
            say('The furniture is already bought.');
          }
          break;
      }
    } else {
      say('You have not enough coins.');
    }
    break;
  case 'no':
    say('Okay.');
    break;
  case 'goodbye': case null: break;
  default:
    say('I do not understand you, I think that we do not speak the same language.');
    break;
}
say('Goodbye.');
