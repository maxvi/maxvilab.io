// To install the web-socket plugin: npm install websocket
plugins = {
  http: require('http'),
  ws: require('websocket')
};
httpServer = plugins.http.createServer(function() {});
httpServer.listen(1234, function() {});
server = new plugins.ws.server({
    httpServer: httpServer
});
idCounter = 0;
clients   = [];
function getClient(ID) {
  for (var i in clients) {
    if (clients[i].ID === ID) {
       return i;
    }
  }
  return -1;
}
function broadcast(msg, place, channel, excl) {
  for(var i in clients) {
    if (clients[i].player.place === place &&
       clients[i].channel      === channel &&
       clients[i].ID           !== excl) {
      clients[i].sendUTF(msg);
    }
  }
}
server.on('request', function(request) {
  var client    = request.accept(null,request.origin);
  client.ID     = idCounter++;
  client.player = {
                    place: 'hometown'
                  };
  clients.push(client);
  client.on('message', function(message) {
    var command = JSON.parse(message.utf8Data);
    switch (command.type) {
      case 'channel':
        client.channel = command.data;
        console.log('Player connected to ' + client.channel);
        break;
      case 'update':
        var oldPlace  = client.player.place;
        client.player = command.data;
        broadcast(JSON.stringify({
                                   type: 'player',
                                   data: client.player,
                                   ID: client.ID
                                 }),
                  client.player.place,
                  client.channel,
                  client.ID);
        if (oldPlace !== client.player.place) {
          for (var i in clients) {
            if (clients[i].player.place === oldPlace) {
              clients[i].sendUTF(JSON.stringify({
                                                  type: 'remove',
                                                  data: client.ID
                                                }));
            }
          }
        }
        break;
      case 'version':
        if (command.data !== '2') {
          client.sendUTF(JSON.stringify({
                                          type: 'error',
                                          data: 'outdated'
                                        }));
          client.close();
        }
        break;
    }
    clients[getClient(client.ID)] = client;
  });
  client.on('close', function() {
    console.log('Player disconnected from ' + client.channel);
    for (var i in clients) {
      if (clients[i].player.place === client.player.place) {
        clients[i].sendUTF(JSON.stringify({
                                            type: 'remove',
                                            data: client.ID
                                          }));
      }
    }
    clients.splice(getClient(client.ID), 1);
  });
});
