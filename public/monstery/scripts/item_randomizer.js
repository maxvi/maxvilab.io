var x = random(0, background.width),
    y = random(0, background.height);
while(solidCollision({x: x,
                      y: y,
                      width: player.sprite.width,
                      height: player.sprite.height
                     })) {
  x = random(0, background.width);
  y = random(0, background.height);
}
player.x = x;
player.y = y;
