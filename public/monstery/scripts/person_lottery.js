say('Hello player!');
switch (ask('Did you want to buy a lottery ticket?')) {
  case 'yes':
    var price = parseInt(ask('What will you like to pay for a ticket?')),
    maxPrize  = ~~(7000/price),
    prize     = random(0,maxPrize);
    if (price <= gameStorage.coins && price >= 1 && !isNaN(price)) {
      say('Here is your ticket!');
      gameStorage.coins -= price;
      say('Did you ticket have a prize?');
      say('Let me see your ticket.');
      say('Hmm...');
      if (prize==maxPrize) {
        say('Gold jakpot!!!! 2000 coins. You are rich!');
        gameStorage.coins += 2000;
      } else if (prize > maxPrize - 10) {
        say('Silver jakpot!!!! 200 coins.');
        gameStorage.coins += 200;
      } else if (prize < 10) {
        say('You have win 30 coins.');
        gameStorage.coins += 30;
      } else if (prize<20) {
        say('You have win 15 coins.');
        gameStorage.coins += 15;
      } else if (random(0, maxPrize * 5) === maxPrize * 5) {
        say('Diamond jakpot!!!! 100000 coins. You are rich!');
      }
      else {
        say('Nothing :(.');
      }
    } else if (isNaN(price)) {
      say('Please type a number.');
    } else if (price < 1) {
      say('The minimum price is 1.');
    } else {
      say('You have not enough coins.');
    }
    break;
  case 'no':
    say('Come back another time.');
    break;
  case null: break;
  default:
    say('Ohh... I do not understand you, I think that we do not speak the same language.');
    break;
}
say('Goodbye.');
