blackOpacity    = 1;
fades           = false;
music.volume    = 0;
var speedBefore = player.speed;
player.speed    = 0;
player.health   = player.maxHealth = 100 * gameStorage.upgrades[3];
sounds.sleep.play();
setTimeout(function() {
  fadeOut();
  music.volume = 1;
  player.speed = speedBefore;
}, 1000 * sounds.sleep.duration);
