if (!document.createElement('canvas').getContext && !document.createElement('canvas').getContext('2d')) {
  document.write('Your browser does not support HTML5 canvas, please install a mordern browser to play Monstery\n\n<a href = "http://chrome.google">Download Google Chrome!</a>');
}
var isTouch  = 'ontouchstart' in document,
graphics     = document.getElementById('graphics').getContext('2d'),
sprites      = {},
sounds       = {},
maps         = {},
scripts      = {},
version      = 2,
pixelSize    = 5,
mouse        = {
                 x: 0,
                 y: 0
               },
blackOpacity = 0,
fades        = false,
touches      = [];
graphics.canvas.focus();
loadResources([
                // Preloaded sprites
                'player_boy',
                'tie_black',
                // Sprites
                'background_arena.darkness',
                'background_arena.graveyard',
                'background_arena.nature',
                'background_arena.snowland',
                'background_arena.stoneland',
                'background_peaceful.club',
                'background_peaceful.furniturestore',
                'background_peaceful.home',
                'background_peaceful.hometown',
                'background_peaceful.upgradeshop',
                'background_survival.desert',
                'blood_machine',
                'blood_normal',
                'blood_slime',
                'blood_snow',
                'button_back',
                'button_move',
                'foreground_arena.darkness',
                'item_coin',
                'item_damage',
                'item_explosion',
                'item_health',
                'item_randomizer',
                'item_speed',
                'monster_ghost',
                'monster_lavaboss',
                'monster_robot',
                'monster_slime',
                'monster_snowman',
                'monster_vampire',
                'monster_waterboss',
                'monster_zombie',
                'object_bed',
                'object_chair',
                'object_club',
                'object_couch',
                'object_crucifix',
                'object_exit',
                'object_fire',
                'object_furniturestore',
                'object_grave',
                'object_home',
                'object_lava',
                'object_ruby',
                'object_snowtree',
                'object_stone',
                'object_teleporter1',
                'object_teleporter2',
                'object_torch',
                'object_tree',
                'object_tv',
                'object_upgradeshop',
                'object_vase',
                'object_water',
                'object_wood',
                'person_aaron',
                'person_max',
                'person_seller',
                'person_sensei',
                'person_villager',
                'player_girl',
                'projectile_shoot',
                'stonewall',
                'tie_blue',
                'tie_brown',
                'tie_green',
                'tie_orange',
                'tie_white',
                'tie_yellow'
              ],
              [ // Sounds
                'break',
                'click',
                'damage',
                'music_arena.darkness',
                'music_arena.graveyard',
                'music_arena.nature',
                'music_arena.snowland',
                'music_arena.stoneland',
                'music_peaceful.club',
                'music_peaceful.furniturestore',
                'music_peaceful.home',
                'music_peaceful.hometown',
                'music_peaceful.upgradeshop',
                'music_survival.desert',
                'pickup',
                'shoot',
                'sleep',
                'walk1',
                'walk2',
                'walk3'
              ],
              [ // Maps
                'arena.darkness',
                'arena.graveyard',
                'arena.nature',
                'arena.snowland',
                'arena.stoneland',
                'peaceful.club',
                'peaceful.furniturestore',
                'peaceful.home',
                'peaceful.hometown',
                'peaceful.upgradeshop',
                'survival.desert'
              ],
              [ // Scripts
                'item_coin',
                'item_damage',
                'item_explosion',
                'item_health',
                'item_randomizer',
                'item_speed',
                'object_bed',
                'object_goto',
                'object_notouch',
                'object_ruby',
                'object_tv',
                'onload_arena.stoneland',
                'onload_peaceful.home',
                'onload_peaceful.hometown',
                'person_aaron',
                'person_credits',
                'person_gender',
                'person_lottery',
                'person_max',
                'person_multiplayer',
                'person_quiz',
                'person_seller',
                'person_sensei',
                'projectile_shoot'
              ]);
ontouchstart = onmousedown = function(event) {
  event.preventDefault();
  if (loadingPercentage       === 100 &&
      (isTouch && event.type  === 'touchstart') ||
      (!isTouch && event.type === 'mousedown')) {
    updateMouse(event);
    switch (UI) {
      case 'game':
        if (isTouch) {
          if (mouseCollision({
                               x: 10,
                               y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                               width: graphics.canvas.width / 12,
                               height: graphics.canvas.width / 12
                             })) {
            player.velocity.x = -player.speed;
          }
          if (mouseCollision({
                               x: 10 + graphics.canvas.width / 6,
                               y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                               width: graphics.canvas.width / 12,
                               height: graphics.canvas.width / 12
                             })) {
            player.velocity.x = player.speed;
          }
          if (mouseCollision({
                               x: 10 + graphics.canvas.width / 12,
                               y: graphics.canvas.height - graphics.canvas.width / 12 * 3 - 10,
                               width: graphics.canvas.width / 12,
                               height: graphics.canvas.width / 12
                             })) {
            player.velocity.y = -player.speed;
          }
          if (mouseCollision({
                               x: 10 + graphics.canvas.width / 12,
                               y: graphics.canvas.height - graphics.canvas.width / 12 - 10,
                               width: graphics.canvas.width / 12,
                               height: graphics.canvas.width / 12
                             })) {
            player.velocity.y = player.speed;
          }
          if (!mouseCollision({
                                x: 10,
                                y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                                width: graphics.canvas.width / 12,
                                height: graphics.canvas.width / 12
                              }) &&
              !mouseCollision({
                                x: 10 + graphics.canvas.width / 6,
                                y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                                width: graphics.canvas.width / 12,
                                height: graphics.canvas.width / 12
                              }) &&
              !mouseCollision({
                                x: 10 + graphics.canvas.width / 12,
                                y: graphics.canvas.height - graphics.canvas.width / 12 * 3 - 10,
                                width: graphics.canvas.width / 12,
                                height: graphics.canvas.width / 12
                              }) &&
              !mouseCollision({
                                x: 10 + graphics.canvas.width / 12,
                                y: graphics.canvas.height - graphics.canvas.width / 12 - 10,
                                width: graphics.canvas.width / 12,
                                height: graphics.canvas.width / 12
                              }) &&
              !player.dead) {
            shoot({
                    x: player.x,
                    y: player.y,
                    velocity: velocity({
                                        x: player.x,
                                        y: player.y
                                       },
                                       {
                                         x: mouse.x + camera.x,
                                         y: mouse.y + camera.y
                                       },
                                      10
                                      )
                  },
                  'player'
                 );
          }
        } else if (!player.dead) {
          shoot({
                  x: player.x,
                  y: player.y,
                  velocity: velocity({
                                       x: player.x,
                                       y: player.y
                                     },
                                     {
                                       x: mouse.x + camera.x,
                                       y: mouse.y + camera.y
                                     },
                                     10)
                },
                'player');
        }
        break;
      case 'credits':
        if (mouseCollision({
                             x: graphics.canvas.width - sprites.button_back.width - 3,
                             y: 3,
                             width: sprites.button_back.width,
                             height: sprites.button_back.height
                           })) {
          UI = 'game';
          sounds.click.play();
        }
        break;
    }
  }
};
onmouseup  = resetMouse;
ontouchend = function(event) {
  if (UI === 'game' && !player.dead) {
    if (mouseCollision({
                         x: 10,
                         y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                         width: graphics.canvas.width / 12,
                         height: graphics.canvas.width / 12
                       }) &&
        player.velocity.x < 0 ||
        mouseCollision({
                         x: 10 + graphics.canvas.width / 6,
                         y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                         width: graphics.canvas.width / 12,
                         height: graphics.canvas.width / 12
                       }) &&
        player.velocity.x > 0) {
      player.velocity.x = 0;
    }
    if (mouseCollision({
                         x: 10 + graphics.canvas.width / 12,
                         y: graphics.canvas.height - graphics.canvas.width / 12 * 3 - 10,
                         width: graphics.canvas.width / 12,
                         height: graphics.canvas.width / 12
                       }) &&
        player.velocity.y < 0 ||
        mouseCollision({
                         x: 10 + graphics.canvas.width / 12,
                         y: graphics.canvas.height - graphics.canvas.width / 12 - 10,
                         width: graphics.canvas.width / 12,
                         height: graphics.canvas.width / 12
                       }) &&
        player.velocity.y > 0) {
      player.velocity.y = 0;
    }
  }
  resetMouse();
};
ontouchmove = onmousemove = function(event) {
  updateMouse(event);
  if ((isTouch && event.type  === 'touchmove') ||
      (!isTouch && event.type === 'mousemove') &&
      UI                      === 'game') {
    if (!mouseCollision({
                          x: 10,
                          y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                          width: graphics.canvas.width / 12,
                          height: graphics.canvas.width / 12
                        }) &&
                        player.velocity.x < 0 ||
                        !mouseCollision({
                                          x: 10 + graphics.canvas.width / 6,
                                          y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                                          width: graphics.canvas.width / 12,
                                          height: graphics.canvas.width / 12
                                        }) &&
        player.velocity.x > 0) {
      player.velocity.x = 0;
    }
    if (!mouseCollision({
                          x: 10 + graphics.canvas.width / 12,
                          y: graphics.canvas.height - graphics.canvas.width / 12 * 3 - 10,
                          width: graphics.canvas.width / 12,
                          height: graphics.canvas.width / 12
                        }) &&
        player.velocity.y < 0 ||
        !mouseCollision({
                          x: 10 + graphics.canvas.width / 12,
                          y: graphics.canvas.height - graphics.canvas.width / 12 - 10,
                          width: graphics.canvas.width / 12,
                          height: graphics.canvas.width / 12}) &&
        player.velocity.y > 0) {
      player.velocity.y = 0;
    }
  }
};
onkeydown = function(event) {
  if (UI === 'game' && !player.dead) {
    switch (event.which) {
      case 'A'.charCodeAt(0):
      case 37:
        player.velocity.x = -player.speed;
        break;
      case 'W'.charCodeAt(0):
      case 38:
        player.velocity.y = -player.speed;
        break;
      case 'D'.charCodeAt(0):
      case 39:
        player.velocity.x = player.speed;
        break;
      case 'S'.charCodeAt(0):
      case 40:
        player.velocity.y = player.speed;
        break;
      case 27:
        if (gamemode !== 'peaceful' && ask('Are you sure you want to go back hometown?') === 'yes')
        {
          goto('peaceful.hometown');
          gamemode = 'peaceful';
        }
        break;
      case 'R'.charCodeAt(0):
        if (event.ctrlKey) {
          render(true);
        }
        break;
    }
  } else if (event.which === 'L'.charCodeAt(0) && event.ctrlKey) {
    onGameLoaded();
  }
};
onkeyup = function(event)
{
  if (UI === 'game' && !player.dead) {
    switch (event.which) {
      case 'A'.charCodeAt(0):
      case 37:
        if (player.velocity.x < 0) {
          player.velocity.x = 0;
        }
        break;
      case 'D'.charCodeAt(0):
      case 39:
        if (player.velocity.x > 0) {
          player.velocity.x = 0;
        }
        break;
      case 'W'.charCodeAt(0):
      case 38:
        if (player.velocity.y < 0) {
          player.velocity.y = 0;
        }
        break;
      case 'S'.charCodeAt(0):
      case 40:
        if (player.velocity.y > 0) {
          player.velocity.y = 0;
        }
        break;
    }
  }
};
function collision(a, b) {
  a = a === 'player' ? {
                         x: player.x,
                         y: player.y,
                         width: player.sprite.width,
                         height: player.sprite.height
                       } : a;
  a = a === 'player + velocity' ? {
                                    x: player.x + player.velocity.x,
                                    y: player.y + player.velocity.y,
                                    width: player.sprite.width,
                                    height: player.sprite.height
                                  } : a;
  return a.x < b.x + b.width  &&
         a.x + a.width > b.x  &&
         a.y < b.y + b.height &&
         a.y + a.height > b.y;
}
function mouseCollision(obj) {
  if (!isTouch) {
    if (collision(obj,
                  {
                    x: mouse.x,
                    y: mouse.y,
                    width: 1,
                    height: 1
                  })) {
      return true;
    }
  } else {
    for (var i in touches) {
      if (collision(obj,
                    {
                      x: touches[i].x,
                      y: touches[i].y,
                      width: 1,
                      height: 1
                    })) {
        return true;
      }
    }
  }
  return false;
}
function solidCollision(obj, type) {
  for (var i in objects) {
    if (collision(obj,
                  {
                    x: objects[i].x,
                    y: objects[i].y,
                    width: objects[i].sprite.width,
                    height: objects[i].sprite.height
                  })) {
      return true;
    }
  }
  for (i in persons) {
    if (collision(obj,
                  {
                    x: persons[i].x,
                    y: persons[i].y,
                    width: persons[i].sprite.width,
                    height: persons[i].sprite.height
                  }
                 )) {
      return true;
    }
  }
  if (collision(obj,
                {
                  x: -1,
                  y: 0,
                  width: 1,
                  height: background.height
                }) ||
      collision(obj,
                {
                  x: 0,
                  y: -1,
                  width: background.width,
                  height: 1
                }) ||
      collision(obj,
                {
                  x: background.width,
                  y: 0,
                  width: 1,
                  height: background.height
                }) ||
      collision(obj,
                {
                  x: 0,
                  y: background.height,
                  width: background.width,
                  height: 1
                })) {
    return true;
  }
  return false;
}
function random(min, max) {
  return ~~(Math.random() * (max - min + 1) + min);
}
function velocity(a, b, speed) {
  var angle = Math.atan2(b.y - a.y, b.x - a.x);
  return {
           x: Math.cos(angle) * speed,
           y: Math.sin(angle) * speed
         };
}
function render(repeat) {
  update();

  graphics.clearRect(0,
                     0,
                     UI === 'game' ? background.width : graphics.canvas.width,
                     UI === 'game' ? background.height : graphics.canvas.height);

  graphics.fillStyle = 'black';
  graphics.fillRect(0, 0, graphics.canvas.width, graphics.canvas.height);

  switch (UI) {
    case 'game':
      graphics.translate(-camera.x, -camera.y); // Move the camera to the given viewpoint
      graphics.drawImage(background, 0, 0); // Draw background-layer

      for (var i in monsters) {
        if (monsters[i].dead &&
            collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: monsters[i].x,
                        y: monsters[i].y,
                        width: monsters[i].sprite.width,
                        height: monsters[i].sprite.width
                      })) {
          graphics.drawImage(monsters[i].blood, monsters[i].x, monsters[i].y); // If the monster is dead then view blood:
        }
      }

      for (i in objects) {
        if (collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: objects[i].x,
                        y: objects[i].y,
                        width: objects[i].sprite.width,
                        height: objects[i].sprite.width
                      })) {
          graphics.drawImage(objects[i].sprite, objects[i].x, objects[i].y);
        }
      }

      for (i in items) {
        if (collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: items[i].x,
                        y: items[i].y,
                        width: items[i].sprite.width,
                        height: items[i].sprite.width
                      })) {
          graphics.drawImage(items[i].sprite, items[i].x, items[i].y);
        }
      }

      for (i in persons) {
        if (collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: persons[i].x,
                        y: persons[i].y,
                        width: persons[i].sprite.width,
                        height: persons[i].sprite.width
                      }
                     )) {
          graphics.drawImage(persons[i].sprite, persons[i].x, persons[i].y);
        }
      }

      for (i in players) {
        if (collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: players[i].x,
                        y: players[i].y,
                        width: players[i].sprite.width,
                        height: players[i].sprite.width
                      })) {
        graphics.drawImage(players[i].sprite, players[i].x, players[i].y);
        graphics.drawImage(spriteSheet(tie(players[i].level),
                                       players[i].direction,
                                       4,
                                       0,
                                       1),
                           players[i].x,
                           players[i].y);
        }
      }

      if (player.dead) {
        graphics.drawImage(player.blood, player.x, player.y);
      }

      for (i in monsters) {
        if (!monsters[i].dead &&
            collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: monsters[i].x,
                        y: monsters[i].y,
                        width: monsters[i].sprite.width,
                        height: monsters[i].sprite.width
                      })) {
          graphics.drawImage(monsters[i].sprite, monsters[i].x, monsters[i].y);
        }
      }

      if (!player.dead) {
        graphics.drawImage(player.sprite, player.x, player.y);
        graphics.drawImage(spriteSheet(tie(gameStorage.highscore),
                                       player.direction,
                                       4,
                                       0,
                                       1),
                           player.x,
                           player.y);
      }

      for (i in projectiles) {
        if (collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: projectiles[i].x,
                        y: projectiles[i].y,
                        width: projectiles[i].sprite.width,
                        height: projectiles[i].sprite.width
                      }
                     )) {
          graphics.drawImage(rotate(projectiles[i].sprite, projectiles[i].rotation),
                             projectiles[i].x,
                             projectiles[i].y);
        }
      }

      for (i in monsters) {
        if (!monsters[i].dead &&
            collision({
                        x: camera.x,
                        y: camera.y,
                        width: graphics.canvas.width,
                        height: graphics.canvas.height
                      },
                      {
                        x: monsters[i].x,
                        y: monsters[i].y,
                        width: monsters[i].sprite.width,
                        height: monsters[i].sprite.width
                      })) {
          healthBar(monsters[i].x,
                    monsters[i].y,
                    monsters[i].sprite,
                    monsters[i].health,
                    monsters[i].maxHealth);
        }
      }

      if (!player.dead) {
        healthBar(player.x,
                  player.y,
                  player.sprite,
                  player.health,
                  player.maxHealth);
      }

      if (foreground) {
        graphics.drawImage(foreground, 0, 0);
      }
      graphics.translate(camera.x, camera.y);

      for (i in precipitation) {
        graphics.fillStyle = precipitation[i].type === 'snow' ? 'white' : 'blue';
        graphics.fillRect(precipitation[i].x,
                          precipitation[i].y,
                          pixelSize,
                          pixelSize);
      }

      graphics.font      = '20pt Calibri';
      graphics.fillStyle = 'white';
      var text           = 'Place: ' + place + // Place
                           (level !== 0 ? ' (level ' + level + ')' : '') + // Level
                           '   Highscore: ' + gameStorage.highscore + // Highscore
                           '   Coins: ' + gameStorage.coins; // Coins
      graphics.fillText(text, 5, 30);
      graphics.fillStyle   = 'black';
      graphics.globalAlpha = 0.3;
      graphics.strokeText(text, 5, 30);
      graphics.globalAlpha = 1;

      if (isTouch) {
        graphics.globalAlpha = 0.4;
        if (mouseCollision({
                             x: 10,
                             y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                             width: graphics.canvas.width / 12,
                             height: graphics.canvas.width / 12
													 })) {
          graphics.globalAlpha = 0.7;
        }
        graphics.drawImage(rotate(sprites.button_move,
                                  Math.PI),
                           10,
                           graphics.canvas.height - graphics.canvas.width / 6 - 10,
                           graphics.canvas.width / 12,
                           graphics.canvas.width / 12);
        graphics.globalAlpha = 0.4;
        if (mouseCollision({
                             x: 10 + graphics.canvas.width / 6,
                             y: graphics.canvas.height - graphics.canvas.width / 6 - 10,
                             width: graphics.canvas.width / 12,
                             height: graphics.canvas.width / 12
                           })) {
          graphics.globalAlpha = 0.7;
        }
        graphics.drawImage(sprites.button_move,
                           10 + graphics.canvas.width / 6,
                           graphics.canvas.height - graphics.canvas.width / 6 - 10,
                           graphics.canvas.width / 12,
                           graphics.canvas.width / 12);
        graphics.globalAlpha = 0.4;
        if (mouseCollision({
                             x: 10 + graphics.canvas.width / 12,
                             y: graphics.canvas.height - graphics.canvas.width / 12 * 3 - 10,
                             width: graphics.canvas.width / 12,
                             height: graphics.canvas.width / 12
                           })) {
          graphics.globalAlpha = 0.7;
        }
        graphics.drawImage(rotate(sprites.button_move, Math.PI * 1.5),
                           10 + graphics.canvas.width / 12, graphics.canvas.height - graphics.canvas.width / 12 * 3 - 10,
                           graphics.canvas.width / 12,
                           graphics.canvas.width / 12);
        graphics.globalAlpha = 0.4;
        if (mouseCollision({
                             x: 10 + graphics.canvas.width / 12,
                             y: graphics.canvas.height - graphics.canvas.width / 12 - 10,
                             width: graphics.canvas.width / 12,
                             height: graphics.canvas.width / 12
                           })) {
          graphics.globalAlpha = 0.7;
        }
        graphics.drawImage(rotate(sprites.button_move, Math.PI / 2),
                           10 + graphics.canvas.width / 12,
                           graphics.canvas.height - graphics.canvas.width / 12 - 10,
                           graphics.canvas.width / 12,
                           graphics.canvas.width / 12);
        graphics.globalAlpha = 1;
      }
      break;
    case 'credits':
      graphics.fillStyle = graphics.createPattern(sprites.stonewall, 'repeat');
      graphics.fillRect(0,
                        0,
                        graphics.canvas.width,
                        graphics.canvas.height);
      graphics.font = '20pt Calibri';
      graphics.fillStyle = 'white';
      graphics.fillText('Max Vistrup', graphics.canvas.width / 20, 25);
      graphics.font = '15pt Calibri';
      graphics.fillStyle = 'white';
      graphics.fillText('MaXYZW', graphics.canvas.width / 20, 50);
      graphics.font = '25pt Calibri';
      graphics.fillStyle = 'white';
      graphics.fillText('Credits', graphics.canvas.width / 20, 125);
      graphics.font = '15pt Calibri';
      graphics.fillStyle = 'white';
      graphics.fillText('Development and graphics by Max Vistrup',
                        graphics.canvas.width / 20,
                        150);
      graphics.fillText('Music by Aaron Krogh', // Thank you!
                        graphics.canvas.width / 20,
                        175);
      graphics.drawImage(sprites.button_back,
                         graphics.canvas.width - sprites.button_back.width - 3,
                         3);
      break;
    case 'loading':
      if (sprites.player_boy && sprites.tie_black) {
        graphics.drawImage(spriteSheet(sprites.player_boy,
                                       0,
                                       4,
                                       0,
                                       1),
                           graphics.canvas.width / 2 - sprites.player_boy.width / 8,
                           graphics.canvas.height / 2 - sprites.player_boy.height / 2);
        graphics.drawImage(spriteSheet(sprites.tie_black,
                                       0,
                                       4,
                                       0,
                                       1),
                           graphics.canvas.width / 2 - sprites.tie_black.width / 8,
                           graphics.canvas.height / 2 - sprites.tie_black.height / 2);
        healthBar(graphics.canvas.width / 2 - sprites.player_boy.width / 8,
                  graphics.canvas.height / 2 - sprites.player_boy.height / 2,
                  spriteSheet(sprites.player_boy,
                              0,
                              4,
                              0,
                              1),
                  loadingPercentage,
                  100);
      }
      graphics.textAlign = 'start';
      graphics.font      = '15pt Calibri';
      graphics.fillStyle = 'white';
      graphics.fillText('Max Vistrup', 5, 30);
      break;
  }

  graphics.fillStyle   = 'black';
  graphics.globalAlpha = blackOpacity;
  graphics.fillRect(0,
                    0,
                    graphics.canvas.width,
                    graphics.canvas.height);
  graphics.globalAlpha = 1;

  if (repeat) {
    (window.requestAnimationFrame || function(repeater) {
      setTimeout(repeater, 0);
    })(function() {
      render(true);
    });
  }
}
function move() {
  for (var i in objects) {
    if (objects[i].script &&
        collision('player + velocity',
                  {
                    x: objects[i].x, y: objects[i].y,
                    width: objects[i].sprite.width,
                    height: objects[i].sprite.height
                  })) {
      objects[i].script();
    }
  }

  for (i in persons) {
    if (persons[i].script &&
        collision('player + velocity',
                  {
                    x: persons[i].x,
                    y: persons[i].y,
                    width: persons[i].sprite.width,
                    height: persons[i].sprite.height
                  }
    )) {
      persons[i].script();
    }
  }

  if (!player.dead && !solidCollision('player + velocity')) {
    player.x += player.velocity.x;
    player.y += player.velocity.y;
    if (player.velocity.x !== 0 || player.velocity.y !== 0)
    {
      player.direction = getDirection(Math.atan2(player.velocity.y, player.velocity.x));
      player.sprite = spriteSheet(player.spriteSheet, player.direction, 4, 0, 1);
      playWalkSound();
    }

    if (player.x + player.sprite.width > camera.x + graphics.canvas.width ||
        player.y + player.sprite.height > camera.y + graphics.canvas.height ||
        player.x < camera.x ||
        player.y < camera.y) {
      moveCamera(player.x + player.sprite.width / 2,
                 player.y + player.sprite.height / 2,
                 5);
    }
  } else {
    player.velocity.x = 0;
    player.velocity.y = 0;
  }
  updateMultiplayer();
  for (i in monsters) {
    if (monsters[i].dead || player.dead) {
      monsters[i].velocity.x = 0;
      monsters[i].velocity.y = 0;
    } else {
      monsters[i].velocity = velocity(monsters[i],
                                      player,
                                      monsters[i].speed);
      if (!solidCollision({
                            x: monsters[i].x + monsters[i].velocity.x,
                            y: monsters[i].y + monsters[i].velocity.y,
                            width: monsters[i].sprite.width,
                            height: monsters[i].sprite.height
                          }) ||
          monsters[i].type === 'ghost') {
        monsters[i].x += monsters[i].velocity.x;
        monsters[i].y += monsters[i].velocity.y;
        if (monsters[i].velocity.x !== 0 || monsters[i].velocity.y !== 0) {
          monsters[i].sprite = spriteSheet(monsters[i].spriteSheet,
                                           getDirection(Math.atan2(monsters[i].velocity.y, monsters[i].velocity.x)),
                                           4,
                                           0,
                                           1);
        }
      }
      if (!monsters[i].dead &&
          collision('player',
                    {
                      x: monsters[i].x + monsters[i].velocity.x,
                      y: monsters[i].y + monsters[i].velocity.y,
                      width: monsters[i].sprite.width,
                      height: monsters[i].sprite.height
                    })) {
        sounds.damage.play();
        player.health -= monsters[i].damage;
      }
    }
  }
  for (i in projectiles) {
    if (projectiles[i].script) {
      projectiles[i].script();
    }
    if (projectiles[i]) {
      if (projectiles[i].properties.ghost ||
          !solidCollision({
                            x: projectiles[i].x + projectiles[i].velocity.x,
                            y: projectiles[i].y + projectiles[i].velocity.y,
                            width: rotate(projectiles[i].sprite, projectiles[i].rotation).width / 4,
                            height: rotate(projectiles[i].sprite, projectiles[i].rotation).height
                          })) {
        projectiles[i].x += projectiles[i].velocity.x;
        projectiles[i].y += projectiles[i].velocity.y;
        projectiles[i].rotation += projectiles[i].velocity.rotate;
      } else if (projectiles[i].properties.breakable) {
        sounds.break.play();
        projectiles.splice(i, 1);
      }
    }
  }
  for (i in items)
  {
    if (collision(
                  'player',
                  {
                    x: items[i].x,
                    y: items[i].y,
                    width: items[i].sprite.width,
                    height: items[i].sprite.height
                  })) {
      sounds.pickup.play();
      items[i].script();
      items.splice(i, 1);
    }
  }
  for (i in precipitation) {
    if (precipitation[i].type === 'snow') {
      precipitation[i].x += random(-1, 1);
      precipitation[i].y++;
    } else {
      precipitation[i].x++;
      precipitation[i].y += 10;
    }
    if (precipitation[i].y > graphics.canvas.height + pixelSize) {
      precipitation[i].y = -pixelSize;
      precipitation[i].x = random(-pixelSize + 1, graphics.canvas.width);
    }
  }
  gameStorage.x         = player.x;
  gameStorage.y         = player.y;
  gameStorage.health    = player.maxHealth;
  gameStorage.maxHealth = player.maxHealth;
}
function shoot(position, owner) {
  if ((function() {
    var counter = 0;
    for (var i in projectiles) {
      if (projectiles[i].owner === 'player' && projectiles[i].type === 'shoot') {
        counter++;
      }
    }
    return counter;
  })() < gameStorage.upgrades[1] + 4 && level !== 0) {
    sounds.shoot.play();
    projectiles.push({
                       x: position.x,
                       y: position.y,
                       sprite: sprites.projectile_shoot,
                       owner: owner,
                       rotation: 0,
                       velocity: position.velocity,
                       type: 'shoot',
                       script: scripts.projectile_shoot,
                       properties: {
                         breakable: true,
                         ghost: false
                       }
                     });
    if (projectiles[projectiles.length - 1].velocity.x === 0) {
      projectiles[projectiles.length - 1].velocity.x = 3;
    }
    if (projectiles[projectiles.length - 1].velocity.y === 0) {
      projectiles[projectiles.length - 1].velocity.y = random(-3, 3);
    }
    projectiles[projectiles.length - 1].velocity.rotate = 0.1;
  }
}
function readFile(URL, onError) {
  var request = new XMLHttpRequest();
  request.open('GET', URL, false);
  request.onload = function() {
    if (onError && this.status >= 400) {
      onError();
    }
  };
  request.send();
  return request;
}
function moveCamera(moveToX, moveToY, speed) {
  if ('cameraInterval' in window) {
    clearInterval(cameraInterval);
  }
  moveToX = moveToX - graphics.canvas.width / 2;
  moveToY = moveToY - graphics.canvas.height / 2;
  if (moveToX < 0) {
    moveToX = 0;
  }
  if (moveToX > background.width - graphics.canvas.width) {
    moveToX = background.width - graphics.canvas.width;
  }
  if (moveToY < 0) {
    moveToY = 0;
  }
  if (moveToY > background.height - graphics.canvas.height) {
    moveToY = background.height - graphics.canvas.height;
  }
  if (background.width < graphics.canvas.width) {
    moveToX = background.width / 2 - graphics.canvas.width / 2;
  }
  if (background.height < graphics.canvas.height) {
    moveToY = background.height / 2 - graphics.canvas.height / 2;
  }
  moveToX = Math.round(moveToX);
  moveToY = Math.round(moveToY);
  if (!('camera' in window) ||  speed === -1) {
    camera = {
               x: moveToX,
               y: moveToY
             };
  }
  cameraInterval = setInterval(function() {
    if (Math.abs(camera.x - moveToX) <= 3 && Math.abs(camera.y - moveToY) <= 3) {
      clearInterval(cameraInterval);
    } else {
      for (var i = 0; i < speed; i++) {
        if (camera.x !== moveToX || camera.y !== moveToY) {
          if (moveToX < camera.x) {
            camera.x--;
          } else if (moveToX > camera.x) {
            camera.x++;
          }
          if (moveToY < camera.y) {
            camera.y--;
          } else if (moveToY > camera.y) {
            camera.y++;
          }
        }
      }
    }
  }, 1);
}
function update() {
  graphics.canvas.width  = innerWidth;
  graphics.canvas.height = innerHeight;
  if (UI === 'game') {
    localStorage.monstery = JSON.stringify(gameStorage);
    if (music.paused) {
      music.loop = true;
      music.play();
    } if (player.spriteSheet !== sprites['player_' + gameStorage.gender]) {
      player.spriteSheet = sprites['player_' + gameStorage.gender];
      player.sprite      = spriteSheet(player.spriteSheet,
                                       0,
                                       4,
                                       0,
                                       1);
    }
    if (player.health <= 0 && !player.dead) {
      gameStorage.coin--;
      player.dead = true;
      if (level > gameStorage.highscore) {
        gameStorage.highscore = level;
        say('NEW HIGHSCORE!!! ' + level);
      } else {
        say('Game over ;(');
      }
      setTimeout(function() {
        if (gamemode === 'survival') {
          gameStorage.coins += level;
        }
        goto('peaceful.hometown');
        player.dead = false;
        player.health = player.maxHealth = 100 * gameStorage.upgrades[3];
      }, 2000);
    }
    for (var i in monsters) {
      if (monsters[i] && monsters[i].health <= 0) {
        monsters[i].dead = true;
        switch (gamemode) {
          case 'arena':
            if ((function() {
              var counter = 0;
              for (var i in monsters) {
                if (!monsters[i].dead) {
                  counter++;
                }
              }
              return counter;
            })() === 0) {
              if (level === (function() {
                maxLevel = 0;
                for (var i in maps) {
                  if (maps[i].level > maxLevel && i.substr(0, 6) === 'arena.') {
                    maxLevel = maps[i].level;
                  }
                }
                return maxLevel;
              })()) {
                gameStorage.highscore = level;
                goto('peaceful.home');
                persons.push({
                               x: 540,
                               y: 235,
                               sprite: sprites.person_sensei,
                               type: 'sensei',
                               script: scripts.person_sensei,
                               properties: {}
                             });
              } else {
                goto((function() {
                  for (var i in maps) {
                    if (maps[i].level === level + 1 && i.substr(0, 6) === 'arena.') {
                      return i;
                    }
                  }
                })());
              }
            }
            break;
          case 'survival':
            if ((function() {
              var counter = 0;
              for (var i in monsters) {
                if (!monsters[i].dead) {
                  counter++;
                }
              }
              return counter;
            })() === 0 && !window.monsterCreationStarted) {
              level++;
              for (i = 0; i < 2; i++) {
                var type;
                switch (random(0, 4)) {
                  case 0:
                    type = 'damage';
                    break;
                  case 1:
                    type = 'explosion';
                    break;
                  case 2:
                    type = 'health';
                    break;
                  case 3:
                    type = 'randomizer';
                    break;
                  case 4:
                    type = 'speed';
                    break;
                }
                items.push({
                             x: random(0, background.width - sprites['item_' + type].width),
                             y: random(0, background.height - sprites['item_' + type].height),
                             sprite: sprites['item_' + type],
                             type: type,
                             script: scripts['item_' + type]
                           });
              }
              monsterCreationStarted = true;
              setTimeout(function() {
                monsterCreationStarted = false;
                monsters = [];
                var type;
                switch (random(0, 2)) {
                  case 0:
                    type = 'vampire';
                    break;
                  case 1:
                    type = 'zombie';
                    break;
                  case 2:
                    type = 'ghost';
                    break;
                }
                for (var i = 0; i < Math.pow(2, level - 1); i++) {
                  monsters.push({
                                  x: random(0, background.width - sprites['monster_' + type].width / 4),
                                  y: random(0, background.height - sprites['monster_' + type].height),
                                  spriteSheet: sprites['monster_' + type],
                                  sprite: spriteSheet(sprites['monster_' + type], 0, 4, 0, 1),
                                  dead: false,
                                  damage: 1,
                                  speed: 1.2,
                                  health: 1,
                                  maxHealth: 1,
                                  blood: blood(type),
                                  velocity: {x: 0, y: 0},
                                  type: type
                                 });
                }
              }, 5000);
            }
            break;
        }
      }
    }
  }
}
function getDirection(angle) {
  switch (Math.round(Math.PI / 5*(function() {
    if (angle < 0) {
      return angle + Math.PI * 2;
    } else return angle;
  })()))
  {
    case 4:
    case 0:
      return 3;
    case 1:
      return 0;
    case 2:
      return 2;
    case 3:
      return 1;
  }
}
function updateMouse(event) {
  mouse.x = event.pageX;
  mouse.y = event.pageY;
  if (isTouch) {
    touches = [];
    for (var i in event.touches) {
      touches.push({
                     x: event.touches[i].pageX,
                     y: event.touches[i].pageY
                   });
    }
  }
}
function healthBar(x, y, sprite, health, maxHealth) {
  graphics.fillStyle = 'black';
  graphics.fillRect(x + sprite.width / 2 - 50, y - 25, 100, 20);
  if (health > maxHealth / 2) {
    graphics.fillStyle = 'green';
  } else if (health >= maxHealth / 2.5 && health <= maxHealth / 2) {
    graphics.fillStyle = 'yellow';
  } else if (health < maxHealth / 2.5) {
    graphics.fillStyle = 'red';
  }
  graphics.fillRect(x + sprite.width / 2 - 50, y - 25, 100 / maxHealth * health, 20);
  graphics.strokeStyle = 'rgb(43, 43, 43)';
  graphics.strokeRect(x + sprite.width / 2 - 50, y - 25, 100, 20);
}
function resetMouse() {
  touches = [];
  mouse.x = 0;
  mouse.y = 0;
}
function goto(map) {
  gamemode        = map.split('.')[0];
  fadeOut();
  level           = maps[map].level;
  place           = map;
  background      = sprites['background_' + map];
  foreground      = sprites['foreground_' + map];
  music.pause();
  music           = sounds['music_' + map];
  precipitation   = [];
  persons         = [];
  players         = [];
  items           = [];
  objects         = [];
  monsters        = [];
  projectiles     = [];
  gameStorage.map = map;
  for (var i in maps[map].persons) {
    persons.push({
                   x: maps[map].persons[i].x,
                   y: maps[map].persons[i].y,
                   sprite: sprites['person_' + maps[map].persons[i].type],
                   type: maps[map].persons[i].type,
                   script: scripts['person_' + maps[map].persons[i].script],
                   properties: maps[map].persons[i].properties
                 });
  }
  for (i in maps[map].items) {
    items.push({
                 x: maps[map].items[i].x,
                 y: maps[map].items[i].y,
                 sprite: sprites['item_' + maps[map].items[i].type],
                 type: maps[map].items[i].type,
                 script: scripts['item_' + maps[map].items[i].type]
               });
  }
  for (i in maps[map].objects) {
    objects.push({
                   x: maps[map].objects[i].x,
                   y: maps[map].objects[i].y,
                   sprite: sprites['object_' + maps[map].objects[i].type],
                   type: maps[map].objects[i].type,
                   properties: maps[map].objects[i].properties,
                   script: scripts['object_' + maps[map].objects[i].script]
                 });
  }
  for (i in maps[map].monsters) {
    monsters.push({
                     x: maps[map].monsters[i].x,
                     y: maps[map].monsters[i].y,
                     spriteSheet: sprites['monster_' + maps[map].monsters[i].type],
                     sprite: spriteSheet(sprites['monster_' + maps[map].monsters[i].type], 0, 4, 0, 1),
                     dead: false,
                     damage: maps[map].monsters[i].damage,
                     speed: maps[map].monsters[i].speed,
                     health: maps[map].monsters[i].health,
                     maxHealth: maps[map].monsters[i].health,
                     blood: blood(maps[map].monsters[i].type),
                     velocity: {x: 0, y: 0},
                     type: maps[map].monsters[i].type
                  });
  }
  if ((new Date()).getDate() === 24 && (new Date()).getMonth() === 12) {
    createPrecipitation('snow', 150);
  } else if (maps[map].whether) {
    createPrecipitation(maps[map].whether, 150);
  }
  player.x = maps[map].player.x;
  player.y = maps[map].player.y;
  if (scripts['onload_' + map]) {
    scripts['onload_' + map]();
  }
  updateMultiplayer();
  moveCamera(player.x, player.y, -1);
}
function onGameLoaded() // This function is called when the game is started
{
  if (!window.localStorage) {
    localStorage = {};
    say('Storage is not supported. Your game data will not be saved when your close Monstery.');
  }
  if (!localStorage.monstery) {
    localStorage.monstery = '{}';
  }
  gameStorage = JSON.parse(localStorage.monstery)
  if (!gameStorage.highscore) {
    gameStorage.highscore = 1;
  }
  if (!gameStorage.coins) {
    gameStorage.coins = 20;
  }
  if (!gameStorage.upgrades) {
    gameStorage.upgrades = [1, 1, 1, 1];
  }
  if (!gameStorage.version) {
    gameStorage.version = version;
  }
  if (!gameStorage.bag) {
    gameStorage.bag = [];
  }
  if (!gameStorage.furnitures) {
    gameStorage.furnitures = [];
  }
  if (!gameStorage.map) {
    gameStorage.map = 'peaceful.hometown';
  }
  if (!gameStorage.gender) {
    switch (ask('Will you be a girl or a boy?')) {
      case 'boy':
        gameStorage.gender = 'boy';
        break;
      case 'girl':
        gameStorage.gender = 'girl';
        break;
      default:
        gameStorage.gender = random(0, 1) ? 'boy' : 'girl';
        break;
    }
  }
  if (!gameStorage.checkpoints) {
    gameStorage.checkpoints = {
      playedBefore: false,
      quizLastPlayed: 0
    };
  }
  gameStorage.version = version;
  render(true);
  if (!gameStorage.checkpoints) {
    say('Long ago... When the monsters had invaded Japan, but not all of Japan.');
    say('In a small town in Japan where there were no monsters were a strong ninja borns.');
    say('The strong ninja is YOU!'); // :D
    say('The ninja fighted against the monsters and tried to get Japan back and kill all of the monsters.');
    say('Welcome to Monstery.\nCreated by Max Vistrup.');
    gameStorage.checpoints.playedBefore = true;
  }
  music = sounds['music_peaceful.hometown'];
  player = {
    x: null,
    y: null,
    dead: false,
    spriteSheet: sprites['player_' + gameStorage.gender],
    sprite: spriteSheet(sprites['player_' + gameStorage.gender], 0, 4, 0, 1),
    blood: sprites.blood_normal,
    speed: 4 * gameStorage.upgrades[2],
    health: 100 * gameStorage.upgrades[3],
    maxHealth: 100 * gameStorage.upgrades[3],
    damage: gameStorage.upgrades[0],
    velocity: {x: 0, y: 0},
    direction: 0
  };
  multiplayer = {};
  goto(gameStorage.map);
  if (gameStorage.x) {
    player.x         = gameStorage.x;
    player.y         = gameStorage.y;
    player.health    = gameStorage.health;
    player.maxHealth = gameStorage.maxHealth;
  }
  moveInterval = setInterval(move, 1000 / 30);
  UI = 'game';
}
function fadeOut() {
  if (!fades) {
    fades = true;
    blackOpacity = 1;
    fadeInterval = setInterval(function() {
      if (blackOpacity <= 0)
      {
        clearInterval(fadeInterval);
        blackOpacity = 0;
        fades = false;
      }
      else blackOpacity -= 0.01;
    }, 1);
  }
}
function say(msg) {
  alert(msg);
}
function ask(question) {
  var answer = prompt(question);
  return answer ? answer.toLowerCase().replace(/\.|,|!/g, '') : null;
}
function rotate(sprite, angle) {
  var rotatedSprite = document.createElement('canvas').getContext('2d'),
  width = sprite.height * Math.abs(Math.sin(angle)) + sprite.width * Math.abs(Math.cos(angle)),
  height = sprite.height * Math.abs(Math.cos(angle)) + sprite.width * Math.abs(Math.sin(angle));
  rotatedSprite.canvas.width = width;
  rotatedSprite.canvas.height = height;
  rotatedSprite.translate(width / 2, height / 2);
  rotatedSprite.rotate(angle);
  rotatedSprite.translate(-width / 2, -height / 2);
  rotatedSprite.drawImage(sprite, width / 2 - sprite.width / 2, height / 2 - sprite.height / 2);
  return rotatedSprite.canvas;
}
function tie(level) {
  switch (level) {
    case 1:
      return sprites.tie_white;
    case 2:
      return sprites.tie_yellow;
    case 3:
      return sprites.tie_orange;
    case 4:
      return sprites.tie_green;
    case 5:
      return sprites.tie_blue;
    case 6:
      return sprites.tie_brown;
    default:
      return sprites.tie_black;
  }
}
function spriteSheet(spriteSheet, x, xNumber, y, yNumber) {
  var croppedSpriteSheet           = document.createElement('canvas').getContext('2d');
  croppedSpriteSheet.canvas.width  = spriteSheet.width / xNumber;
  croppedSpriteSheet.canvas.height = spriteSheet.height / yNumber;
  croppedSpriteSheet.drawImage(spriteSheet,
                               spriteSheet.width / xNumber * x,
                               spriteSheet.height / yNumber * y,
                               spriteSheet.width / xNumber, spriteSheet.height / yNumber,
                               0,
                               0,
                               spriteSheet.width / xNumber,
                               spriteSheet.height / yNumber);
  return croppedSpriteSheet.canvas;
}
function playWalkSound() {
  switch (random(0, 1)) {
    case 0:
      sounds.walk1.play();
      break;
    case 1:
      sounds.walk2.play();
      break;
  }
}
function loadResources(loadSprites, loadSounds, loadMaps, loadScripts) {
  loadingPercentage = 0;
  UI = 'loading';
  render(false);
  var resourcesLoaded = 0,
  allResources = loadSprites.length + loadSounds.length + loadMaps.length + loadScripts.length;
  loadSprite(0);
  if (ask('Do you want to disable sounds?\nIt will boost the loading.') !== 'no') {
    for (var i in loadSounds) {
      sounds[loadSounds[i]] = new Audio();
    }
    resourcesLoaded += loadSounds.length;
  } else {
    loadSound(0);
  }
  loadMap(0);
  loadScript(0);
  function resourceLoaded() {
    resourcesLoaded++;
    if (resourcesLoaded === allResources) {
      onGameLoaded();
    }
    loadingPercentage = resourcesLoaded / allResources * 100;
    render(false);
  }
  function loadSprite(number) {
    sprites[loadSprites[number]] = new Image();
    sprites[loadSprites[number]].onload = function(event) {
      for (var i in sprites) {
        if (sprites[i] === this) {
          var before = document.createElement('canvas').getContext('2d'),
          after = document.createElement('canvas').getContext('2d');
          before.canvas.width = this.width;
          before.canvas.height = this.height;
          before.drawImage(this, 0, 0);
          var pixels = before.getImageData(0, 0, this.width, this.height).data;
          after.canvas.width = this.width * pixelSize;
          after.canvas.height = this.height * pixelSize;
          for (var x = 0;x <= before.canvas.width;x++) {
            for (var y = 0;y <= before.canvas.height;y++) {
              var px = (y * before.canvas.width + x)*4,
              r = pixels[px],
              g = pixels[px + 1],
              b = pixels[px + 2],
              a = pixels[px + 3] / 255;
              after.fillStyle = 'rgba(' + r + ', ' + g + ', ' + b + ', ' + a + ')';
              after.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
            }
          }
          sprites[i] = after.canvas;
          resourceLoaded();
          if (number < loadSprites.length - 1) {
            loadSprite(number + 1);
          }
        }
      }
    };
    sprites[loadSprites[number]].onerror = function(event) {
      console.error('Sprite not loaded: ' + (function(sprite) {
        for (var i in sprites) {
          if (sprites[i] === sprite) {
            return i;
          }
        }
      })(this));
    };
    sprites[loadSprites[number]].src = 'sprites/' + loadSprites[number] + '.png';
  }
  function loadSound(number) {
    sounds[loadSounds[number]] = new Audio('sounds/' + loadSounds[number] + '.mp3');
    sounds[loadSounds[number]].addEventListener('canplaythrough', function() {
      resourceLoaded();
      if (number < loadSounds.length - 1) {
        loadSound(number + 1);
      }
    });
    sprites[loadSprites[number]].onerror = function(event) {
      console.error('Sound not loaded: ' + (function(sound) {
        for (var i in sounds) {
          if (sounds[i] === sound) {
            return i;
          }
        }
      })(this));
    };
  }
  function loadMap(number) {
    try {
      maps[loadMaps[number]] = JSON.parse(readFile('maps/' + loadMaps[number] + '.json', function()
      {
        console.error('Map not loaded: ' + loadMaps[number]);
      }).response);
      resourceLoaded();
      if (number < loadMaps.length - 1) {
        loadMap(number + 1);
      }
    } catch(err) {
      console.error('Parsing error in map ' + number + ' (' + loadMaps[number] + '): ' + err.message);
    }
  }
  function loadScript(number) {
    try {
      scripts[loadScripts[number]] = Function(readFile('scripts/' + loadScripts[number] + '.js', function() {
        console.error('Script not loaded: ' + loadScripts[number]);
      }).response);
      resourceLoaded();
      if (number < loadScripts.length - 1) {
        loadScript(number + 1);
      }
    } catch(err) {
      console.error('Error in script ' + number + ' (' + loadScripts[number] + '): ' + err.message);
    }
  }
}
function createPrecipitation(type, number) {
  for (var i = 0; i < number; i++) {
    precipitation.push({x: random(0, graphics.canvas.width),
                        y: random(0, graphics.canvas.height),
                        type: type});
  }
}
function blood(type) {
  switch (type) {
    case 'lavaboss':
      return sprites.object_lava;
    case 'waterboss':
      return sprites.object_water;
    case 'slime':
      return sprites.blood_slime;
    case 'vampire':
    case 'ghost':
      return document.createElement('img');
    case 'robot':
      return sprites.blood_machine;
    case 'snowman':
      return sprites.blood_snow;
    default:
      return sprites.blood_normal;
  }
}
function distance(a, b) {
  return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}
function updateMultiplayer() {
  if (multiplayer.readyState) {
    multiplayer.send(JSON.stringify({
      type: 'update', data: {
      x: player.x,
      y: player.y,
      gender: gameStorage.gender,
      direction: player.direction,
      place: place,
      level: gameStorage.highscore
    }}));
  }
}
