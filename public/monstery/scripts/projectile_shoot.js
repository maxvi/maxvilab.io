if (this.owner === 'player' || this.owner === 'explosion') {
  for (var i in monsters) {
    if (collision({
                    x: monsters[i].x,
                    y: monsters[i].y,
                    width: monsters[i].sprite.width,
                    height: monsters[i].sprite.height
                  },
                  {
                    x: this.x,
                    y: this.y,
                    width: rotate(this.sprite,this.rotation).width,
                    height: rotate(this.sprite,this.rotation).height
                  }) && 
        !monsters[i].dead) {
      if (monsters[i].type !== 'lavaboss' && monsters[i].type !== 'waterboss' || random(0,1)) {
        sounds.damage.play();
        monsters[i].health -= player.damage;
        projectiles.splice(projectiles.indexOf(this), 1);
      } else {
        this.owner = 'monster';
        this.velocity = velocity(this, player, 10);
        this.velocity.rotate = 0.5;
      }
    } else if (this.owner === 'monster') {
      if (collision('player',
                    {
                      x: this.x,
                      y: this.y,
                      width: rotate(this.sprite,this.rotation).width,
                      height: rotate(this.sprite,this.rotation).height
                    })) {
        sounds.damage.play();
        projectiles.splice(projectiles.indexOf(this), 1);
        player.health -= player.damage * 30;
      }
    }
  }
}
