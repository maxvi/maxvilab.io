say('Hello player.');
if (gameStorage.checkpoints.quizLastPlayed + 3600000 <= Date.now()) {
  switch (ask('Did you know something about Monstery?')) {
    case 'yes':
      switch (ask('Would you like to try a quiz about Monstery?')) {
        case 'yes':
          var points = 0,
              questions = [];
          gameStorage.checkpoints.quizLastPlayed = Date.now();
          say('The first question is...');
          for (var i = 0;i < 6;i++) {
            while (questions.indexOf(question) !== -1) {
              var question = random(0,7);
            }
            questions.push(question);
            switch (question) {
              case 1:
                switch ((ask('How many monsters are there in the first level of the Monstery arena, the level of darkness?') + '').replace(/ |monsters/gi, '')) {
                  case '2':
                    points += 3;
                    say('Correct! You get 3 points.');
                    break;
                  case 'null':
                    points -= 3;
                    say('Question is skipped. You lose 3 points.');
                    break;
                  default:
                    points -= 3;
                    say('Wrong answer. :( You lose 3 point.');
                    break;
                }
                break;
              case 2:
                switch (ask('What is the name of the creator?')) {
                  case 'max':
                    points += 4;
                    say('Correct! You get 4 points.');
                    break;
                  case 'null':
                    points -= 4;
                    say('Question is skipped. You lose 4 points.');
                    break;
                  default:
                    points -= 3;
                    say('Wrong answer. :( You lose 3 point. This game is created by Max.');
                    break;
                }
                break;
              case 3:
                switch((ask('What cost the ruby?') + '').replace(/ |coin|s|$/gi, '')) {
                  case '100000':
                    points += 5;
                    say('Correct! You get 5 points.');
                    break;
                  case 'null':
                    points -= 5;
                    say('Question is skipped. You lose 5 points.');
                    break;
                  default:
                    points -= 3;
                    say('Wrong answer. :( You lose 3 point.');
                    break;
                }
                break;
              case 4:
                switch((ask('What is the speed of shoots?') + '').replace(/ |speed|of|shoot|shoots/gi, '')) {
                  case '10':
                    points += 20;
                    say('Correct! You get 20 points.');
                    break;
                  case 'null':
                    points -= 20;
                    say('Question is skipped. You lose 20 points.');
                    break;
                  default:
                    points -= 3;
                    say('Wrong answer. :( You lose 3 point.');
                    break;
                }
                break;
              case 5:
                switch(ask('What programming language is Monstery coded in?')) {
                  case 'html5':
                  case 'javascript':
                  case 'js':
                    points += 10;
                    say('Correct! You get 30 points.');
                    break;
                  case null:
                    points -= 10;
                    say('Question is skipped. You lose 10 points.');
                    break;
                  default:
                    points -= 3;
                    say('Wrong answer. :( You lose 3 point.');
                    break;
                }
                break;
            }
          }
          say('The quiz is finished! You get...');
          say(points + ' points' + (points < 5 ? '!' : '.'));
          if (points > 0) {
            say('You have earn ' + points + ' coins.');
            gameStorage.coins += points;
          }
          else say('You lose the quiz. :(');
          break;
        case 'no':
          say('Come back another time, when you want to try the quiz.');
          break;
        case null: break;
        default:
          say('I do not understand you, I think that we do not speak the same language.');
          break;
      }
      break;
    case 'no':
      say('Come back another time, when you have learn somthing about the Monstery and try the quiz.');
      break;
    case null: break;
    default:
      say('I do not understand you, I think that we do not speak the same language.');
      break;
  }
} else {
  say('You can only try the quiz once per hour.');
}
say('Goodbye.');
