say('Hello!');
if (!multiplayer.readyState) {
  switch (ask('Did you want to play multiplayer?')) {
    case 'yes':
      IP                 = prompt('Enter the servers IP-address:');
      multiplayerTimeout = setTimeout(function() {
        say('Hmm... The server is down ;(');
        multiplayer = {};
      }, 10000);
      multiplayer = new WebSocket('ws://' + IP + ':1234');
      multiplayer.onopen=function(event) {
        multiplayer.send(JSON.stringify({
                                          type: 'channel',
                                          data: ask('Which channel would you like to play on?')
                                        }));
        multiplayer.send(JSON.stringify({
                                          type: 'version',
                                          data: version
                                        }));
        say('Keep in mind that this is just a experimental alpha version of Monstery multiplayer.\nBugs and errors should be expected!');
        clearTimeout(multiplayerTimeout);
      };
      multiplayer.onerror = function(event) {
        say('Error: Connection lost');
      };
      multiplayer.onmessage = function(event) {
        var command = JSON.parse(event.data);
        switch(command.type) {
          case 'player':
            var number = players.length;
            for(var i in players) {
              if (players[i].ID === command.ID) {
                number = i;
              }
            }
            players[number] = {
                                sprite: spriteSheet(command.data.gender=='boy' ?
                                                    sprites.player_boy :
                                                    sprites.player_girl,
                                                    command.data.direction,
                                                    4,
                                                    0,
                                                    1),
                                x: command.data.x,
                                y: command.data.y,
                                ID: command.ID,
                                level: command.data.level
                              };
            break;
          case 'error':
            switch(command.data) { // Errors:
              case 'outdated':
                say('Error: Client outdated.');
                break;
            }
            break;
          case 'remove':
            for(var i in players) {
              if (players[i].ID === command.data) {
                players.splice(i, 1);
              }
            }
            break;
        }
      };
      break;
    case 'no':
      say('Okay.');
      break;
    case null: break;
    default:
      say('I do not understand you, I think that we do not speak the same language.');
      break;
  }
} else {
  say('Multiplayer disconnected.');
  multiplayer.close();
  multiplayer = {};
  players     = [];
}
