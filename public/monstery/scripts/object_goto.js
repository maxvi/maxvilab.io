if (!this.properties.key || gameStorage.bag.indexOf(this.properties.key) !== -1) {
  goto(this.properties.map);
} else {
  say('Hmm... The door is locked.');
}
