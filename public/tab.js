function tabClose(buttonClass, tabcontentClass) {
    var tabcontent = document.getElementsByClassName(tabcontentClass);
    for (var i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    var tablinks = document.getElementsByClassName(buttonClass);
    for (var i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
}

function tab(evt, t, buttonClass, tabcontentClass) {
    tabClose(buttonClass, tabcontentClass)
    document.getElementById(t).style.display = "block";
    evt.currentTarget.className += " active";
}
